﻿using UnityEngine;
using System.Collections.Generic;

//So this didn't work out. Just not enough direct control.





public class Transition
{
	public const float CAMERA_ELASTICITY_IN_SMOOTH = 12;
	public const float CAMERA_ELASTICITY_IN_MAXPATH = 12;
	public const float CAMERA_RADIUS = 0.2f;
	public const float LONG_TRIP_LENGTH = 5f;
	public const float ARRIVED_DISTANCE = 0.4f;


	public enum TransitionType
	{
		SMOOTH,
		JUMP,
		MAXPATH,
		BEST

	}

	public bool Done;

	DesirabilityMap.Point dest;
	TransitionType type;
	Transform toMove;

	public Transition (DesirabilityMap.Point dest, Transform toMove, TransitionType type = TransitionType.BEST)
	{
		this.dest = dest;
		this.toMove = toMove;
		this.type = type;


		if (type == TransitionType.BEST) {
			RaycastHit hit;
			if (Physics.SphereCast(toMove.position, CAMERA_RADIUS * 2f, new Vector3(dest.GetPos().x, toMove.position.y, dest.GetPos().y) - toMove.position, out hit, (new Vector3(dest.GetPos().x, toMove.position.y, dest.GetPos().y) - toMove.position).magnitude, CameraController.OBSTACLE_LAYER)) {
				type = TransitionType.JUMP;
			} else if ((new Vector3(dest.GetPos().x, toMove.position.y, dest.GetPos().y) - toMove.position).magnitude > LONG_TRIP_LENGTH) {
					type = TransitionType.MAXPATH;
				} else {
					type = TransitionType.SMOOTH;
				}
		}

		Debug.Log(type + " transition created!");
	}

	Transition currentSmoothMaxPathTransition;

	public void Move (DesirabilityMap map = null)
	{
		//Debug.Log("doing move");
		if (type == TransitionType.SMOOTH) {
			toMove.position = Vector3.Lerp(toMove.position, new Vector3(dest.GetPos().x, toMove.position.y, dest.GetPos().y), 1f / CAMERA_ELASTICITY_IN_SMOOTH);
			Done |= (dest.GetPos() - new Vector2(toMove.position.x, toMove.position.z)).sqrMagnitude <= ARRIVED_DISTANCE;
		} else if (type == TransitionType.JUMP) {
				toMove.position = new Vector3(dest.GetPos().x, toMove.position.y, dest.GetPos().y);
				Done = true;
			} else if (type == TransitionType.MAXPATH) {
					if (map == null) return;
					if (currentSmoothMaxPathTransition != null && currentSmoothMaxPathTransition.dest == dest && currentSmoothMaxPathTransition.Done) {
						Done = true;
					} else if (currentSmoothMaxPathTransition == null || currentSmoothMaxPathTransition.Done) {
							Queue<DesirabilityMap.Point> q = map.GetLocal(new Vector2(toMove.position.x, toMove.position.z));
							DesirabilityMap.Point next = q.Dequeue();
					
							while (next != dest) {
								if ((dest.GetPos() - next.GetPos()).sqrMagnitude < (dest.GetPos() - new Vector2(toMove.position.x, toMove.position.z)).sqrMagnitude) {
									break;
								}
								next = q.Dequeue();
							}
							currentSmoothMaxPathTransition = new Transition(next, toMove, TransitionType.SMOOTH);

						} else {
							currentSmoothMaxPathTransition.Move();
						}
				}
	}



}
