﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections.Specialized;
using System.Collections;

/// <summary>
/// A Desirability Map implementation based on what I call a "Point-Cloud Graph", where points define the important highs and lows, and then locality defines how
/// an actual value is calculated for every position.
/// </summary>
public class DesirabilityMap
{
	/// <summary>
	/// The position to focus on, the center of the map.
	/// </summary>
	public Vector2 Focus { get; protected set; }

	/// <summary>
	/// How far away is the maximum defined desirability?
	/// </summary>
	public float EdgeDistance { get; protected set; }

	/// <summary>
	/// How far away to look for "Local" values, like the max or to calculate a lerp. Should be pretty small, or calculating actual
	/// desirability could take a very long time relatively.
	/// 
	/// Another way to describe it is that this is the distance from which the influence on desirability from a single point on a Get call will reach 0.
	/// </summary>
	/// <value>The local distance.</value>
	public float LocalDistance {
		get {
			return Mathf.Sqrt(_localSqrDistance);
		}
		protected set {
			_localSqrDistance = value * value;
		}
	}

	float _localSqrDistance;

	/// <summary>
	/// The ambient desirability, what is returned outside of the defined structure
	/// </summary>
	/// <value>The ambient desirability.</value>
	public float AmbientDesirability { get; protected set; }

	/// <summary>
	/// Primary data structure object, contains a single point of desirability.
	/// All maxes and mins are at Points, since any points between them are lerped from the surrounding points.
	/// 
	/// Point is a class instead of a struct because I will need to have multiple copies of a pointer to the same instance.
	/// </summary>
	public class Point
	{
		//named x and z since world coords are x-z for horizontal plane.
		float x, z, desirability;

		//x is distance, z is angle if polar!
		public Point (float x, float z, float desirability)
		{
			this.x = x;
			this.z = z;
			this.desirability = desirability;
		}

		public float Get ()
		{
			return desirability;
		}

		public Vector2 GetPos ()
		{
			return new Vector2(x, z);
		}

		public Vector2 Polar (Vector2 focus)
		{
			//distance, angle
			return new Vector2(Mathf.Sqrt(x * x + z * z), Mathf.Atan2(z, x));
		}
	}

	RBListTree<float, Point> desirabilitySorted;

	public DesirabilityMap (Vector2 focus, float edgeDistance = 5, float localDistance = 0.6f, float ambientDesirability = 0f)
	{
		Focus = focus;
		EdgeDistance = edgeDistance;
		LocalDistance = localDistance;
		AmbientDesirability = ambientDesirability;


		desirabilitySorted = new RBListTree<float, Point>();

	}



	public Point AddPoint (Point p)
	{
		desirabilitySorted.Put(p.Get(), p);

		return p;
	}

	public Point AddPoint (float x, float z, float desirability)
	{
		return AddPoint(new Point(x, z, desirability));
	}


	//returns a desirability-ordered list of local points to the passed in position
	public Queue<Point> GetLocal (Vector2 pos)
	{
		Queue<Point> locals = new Queue<Point>();
		//have to do an exaustive search, not really any way to sort by distance in a tree form unless
		//I wanted to re-sort every frame, which is obviously worse, O(n) vs O(nlogn)
		foreach (Point p in desirabilitySorted.Values()) {
			if (Vector2.SqrMagnitude(pos - p.GetPos()) <= _localSqrDistance) {
				locals.Enqueue(p);
			}
		}

		return locals;
	}


	public Point GetLocalMax (Vector2 worldPos)
	{
		//first item will be biggest, already sorted from RBListTree.Values() call.
		return GetLocal(worldPos).Dequeue();
	}

	public List<Point> GetLocalMaxes (Vector2 worldPos)
	{
		//first item will be biggest, already sorted from RBListTree.Values() call.

		List<Point> l = new List<Point>();
		Queue<Point> q = GetLocal(worldPos);
		int count = 0;
		foreach (var p in q) {
			l.Add(p);
			count++;
			if (count >= q.Count / 2f) break;
		}

		return l;
	}


	public Point GetGlobalMax ()
	{
		return GetGlobalMax(Focus);
	}

	public Point GetGlobalMax (Vector3 closeToThis)
	{
		//what to do if there are multiple global maxes?
		//default to one nearest center/focus
		Point r = null;
		float dis = float.PositiveInfinity;
		foreach (var p in desirabilitySorted.MaxValues()) {
			//if this is first run, select this p, otherwise calculate the distance
			float d = r == null ? float.NegativeInfinity : Vector2.Distance(new Vector2(closeToThis.x, closeToThis.z), r.GetPos());
			if (r == null || d < dis) {
				r = p;
				dis = d;
			}
		}

		return r;
	}



	public float GetDesirability (Vector2 worldPos)
	{
		Queue<Point> locs = GetLocal(worldPos);

		if (locs.Count == 0) return AmbientDesirability;


		//perhaps use a cutoff, after desirability gets low enough not to effect the outcome.
		// or only use top so-many 
		float desirability = 0;
		float e = LocalDistance; //only do sqrt calc once
		int s = locs.Count;

		foreach (Point p in locs) {
			//scale each point's desirability by it's distance from the center to local's edge, and then add it.
			desirability += p.Get() * Mathf.Lerp(1, 0, Vector2.Distance(worldPos, p.GetPos()) / e);

			//could cut off after 25% through locs or something
			//if this is a performance problem
		}

		//average out all the weighted additions and normalize to 0-1 range.
		return desirability / s;
	}

	public float GetDesirability (float worldX, float worldZ)
	{
		return GetDesirability(new Vector2(worldX, worldZ));
	}

	public float GetPolarDesirability (float distance, float angle)
	{
		return GetDesirability(new Vector2(Focus.x + Mathf.Cos(Mathf.Deg2Rad * angle) * distance, Focus.y + Mathf.Sin(Mathf.Deg2Rad * angle) * distance));
	}
}
