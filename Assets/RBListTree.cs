﻿using System;
using System.Collections.Generic;

/// <summary>
/// A red-black tree implementation adapted from Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne, modified to only permit insertion and allow multiple values associated with each key,
/// as well as ported from Java to C#.
/// </summary>
public class RBListTree<TKey, TValue>
	where TKey : IComparable<TKey>
{

	const bool RED = true;
	const bool BLACK = false;

	Node root;
	// root of the BST

	// BST helper node data type
	class Node
	{

		public TKey Key;
		// TKey
		public List<TValue> Vals;
		// associated data
		public Node Left, Right;
		// links to left and right subtrees
		public bool Color;
		// color of parent link
		public int N;
		// subtree count

		public Node (TKey key, TValue val, bool color, int n)
		{
			this.Key = key;
			this.Vals = new List<TValue>(2);
			this.Color = color;
			this.N = n;

			Add(val);
		}

		public void Add (TValue val)
		{
			Vals.Add(val);
		}

		public void Remove (TValue val)
		{
			Vals.Remove(val);
		}

		public int Count ()
		{
			return Vals.Count;
		}
	}

	static bool IsRed (Node x)
	{
		if (x == null) return false;
		return x.Color == RED;
	}

	// number of node in subtree rooted at x; 0 if x is null
	static int Size (Node x)
	{
		return x == null ? 0 : x.N;
	}

	public int Size ()
	{
		return Size(root);
	}

	public bool IsEmpty ()
	{
		return root == null;
	}

	public List<TValue> Get (TKey key)
	{
		return Get(root, key);
	}

	static List<TValue> Get (Node x, TKey key)
	{
		while (x != null) {
			int cmp = key.CompareTo(x.Key);
			if (cmp < 0) x = x.Left;
			else if (cmp > 0) x = x.Right;
				else return x.Vals;
		}

		//didn't find
		return null;
	}

	public bool Contains (TKey key)
	{
		return !Equals(Get(key), default(TValue));
	}

	public void Put (TKey key, TValue val /*, bool remove = false*/)
	{
		if (Equals(val, default(TValue))) {

			//if (remove) {
			//remove the val from key's list.
			//not implemented because no use for it as of now,
			//what would we do when last value was removed?
			//would have to modify tree, remove nodes.

			//do stuff
			//}
			//Delete(key);
			//do nothing, there may be other values
			return;
		}

		root = Put(root, key, val);
		root.Color = BLACK;
	}

	// insert the TKey-TValue pair in the subtree rooted at h
	Node Put (Node h, TKey key, TValue val)
	{
		if (h == null) return new Node(key, val, RED, 1);

		int cmp = key.CompareTo(h.Key);
		if (cmp < 0) h.Left = Put(h.Left, key, val);
		else if (cmp > 0) h.Right = Put(h.Right, key, val);
			else h.Add(val);

		// fix-up any right-leaning links
		if (IsRed(h.Right) && !IsRed(h.Left)) h = RotateLeft(h);
		if (IsRed(h.Left) && IsRed(h.Left.Left)) h = RotateRight(h);
		if (IsRed(h.Left) && IsRed(h.Right)) FlipColors(h);
		h.N = Size(h.Left) + Size(h.Right) + 1;

		return h;
	}


	// make a left-leaning link lean to the right
	static Node RotateRight (Node h)
	{
		Node x = h.Left;
		h.Left = x.Right;
		x.Right = h;
		x.Color = x.Right.Color;
		x.Right.Color = RED;
		x.N = h.N;
		h.N = Size(h.Left) + Size(h.Right) + 1;
		return x;
	}

	// make a right-leaning link lean to the left
	static Node RotateLeft (Node h)
	{
		Node x = h.Right;
		h.Right = x.Left;
		x.Left = h;
		x.Color = x.Left.Color;
		x.Left.Color = RED;
		x.N = h.N;
		h.N = Size(h.Left) + Size(h.Right) + 1;
		return x;
	}

	// flip the colors of a node and its two children
	static void FlipColors (Node h)
	{
		// h must have opposite color of its two children

		h.Color = !h.Color;
		h.Left.Color = !h.Left.Color;
		h.Right.Color = !h.Right.Color;
	}

	// Assuming that h is red and both h.left and h.left.left
	// are black, make h.left or one of its children red.
	static Node MoveRedLeft (Node h)
	{
		// assert (h != null);
		// assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

		FlipColors(h);
		if (IsRed(h.Right.Left)) {
			h.Right = RotateRight(h.Right);
			h = RotateLeft(h);
			FlipColors(h);
		}
		return h;
	}

	// Assuming that h is red and both h.right and h.right.left
	// are black, make h.right or one of its children red.
	static Node MoveRedRight (Node h)
	{
		// assert (h != null);
		// assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
		FlipColors(h);
		if (IsRed(h.Left.Left)) {
			h = RotateRight(h);
			FlipColors(h);
		}
		return h;
	}

	public int Height ()
	{
		return Height(root);
	}

	int Height (Node x)
	{
		if (x == null) return -1;
		return 1 + Math.Max(Height(x.Left), Height(x.Right));
	}

	public TKey Min ()
	{
		return IsEmpty() ? default(TKey) : Min(root).Key;
	}

	Node Min (Node x)
	{
		return x.Left == null ? x : Min(x.Left);
	}

	//don't handle empty tree case here

	public List<TValue> MaxValues ()
	{
		return Max(root).Vals;
	}

	public TKey MaxKey ()
	{
		return Max(root).Key;
	}

	Node Max (Node x)
	{
		return x.Right == null ? x : Max(x.Right);
	}

	//returns max to min order of all values
	public List<TValue> Values ()
	{
		List<TValue> order = new List<TValue>(Size());
		sort(root, order);

		return order;
	}

	static void sort (Node curr, List<TValue> order)
	{
		if (curr == null) return;
		sort(curr.Right, order);
		//just add all here, keys are equal
		order.AddRange(curr.Vals);
		sort(curr.Left, order);
	}
}