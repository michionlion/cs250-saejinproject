﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Security.Policy;

public class DevCameraMovement : MonoBehaviour
{

	public GameObject Focus;
	public bool ResetPosition = false;
	public float Speed = 5;
	public KeyCode DevMovementToggle = KeyCode.F1;

	Quaternion startingQ;
	Vector3 startingP;

	//mouselook

	Vector2 _mouseAbsolute;
	Vector2 _smoothMouse;

	public Vector2 ClampInDegrees = new Vector2(360, 180);
	public Vector2 Sensitivity = new Vector2(2, 2);
	public Vector2 Smoothing = new Vector2(1.6f, 1.6f);
	public Vector2 TargetDirection;


	public void Start ()
	{
		if (!Focus) Focus = Camera.main.gameObject;
		startingQ = Focus.transform.rotation;
		startingP = Focus.transform.position;

		// Set target direction to the camera's initial orientation.
		TargetDirection = Focus.transform.localRotation.eulerAngles;
	}

	bool moving;

	public void Update ()
	{

		if (!moving && Input.GetKeyDown(DevMovementToggle)) {
			moving = true;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			CameraController.Enabled = false;

			if (ResetPosition) {
				startingQ = Focus.transform.rotation;
				startingP = Focus.transform.position;
			}

			Globals.PlayerCanMove = false;
			Input.ResetInputAxes();

		} else if (moving && Input.GetKeyDown(DevMovementToggle)) {
				moving = false;
				//reset mouse
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				CameraController.Enabled = true;



				//reset pos and rot
				if (ResetPosition) {
					Focus.transform.rotation = startingQ;
					Focus.transform.position = startingP;
				}

				Globals.PlayerCanMove = true;
				Input.ResetInputAxes();
			}


		if (moving) {
			Rotate();
			Move();
		}

	}

	public void Move ()
	{
		
		float vAx = Input.GetAxis("Vertical") * Time.deltaTime * Speed;
		float hAx = Input.GetAxis("Horizontal") * Time.deltaTime * Speed;
		float uAx = Input.GetAxis("Height") * Time.deltaTime * Speed;
		Focus.transform.Translate(hAx, uAx, vAx, Space.Self);
	}

	public void Rotate ()
	{

		// Allow the script to clamp based on a desired target value.
		var targetOrientation = Quaternion.Euler(TargetDirection);

		// Get raw mouse input for a cleaner reading on more sensitive mice.
		var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

		// Scale input against the sensitivity setting and multiply that against the smoothing value.
		mouseDelta = Vector2.Scale(mouseDelta, new Vector2(Sensitivity.x * Smoothing.x, Sensitivity.y * Smoothing.y));

		// Interpolate mouse movement over time to apply smoothing delta.
		_smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / Smoothing.x);
		_smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / Smoothing.y);

		// Find the absolute mouse movement value from point zero.
		_mouseAbsolute += _smoothMouse;

		// Clamp and apply the local x value first, so as not to be affected by world transforms.
		if (ClampInDegrees.x < 360) _mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -ClampInDegrees.x * 0.5f, ClampInDegrees.x * 0.5f);

		var xRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, targetOrientation * Vector3.right);
		Focus.transform.localRotation = xRotation;

		// Then clamp and apply the global y value.
		if (ClampInDegrees.y < 360) _mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -ClampInDegrees.y * 0.5f, ClampInDegrees.y * 0.5f);

		Focus.transform.localRotation *= targetOrientation;

		// If there's a character body that acts as a parent to the camera

		var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, Focus.transform.InverseTransformDirection(Vector3.up));
		Focus.transform.localRotation *= yRotation;

	}
}