﻿using System;

public static class Globals
{


	public enum EVENT
	{
		PlayerMoveUpdate,
		PlayerUpdate,
		SceneChange

	}

	static Action<EVENT> eventListeners;



	static bool playerMove = true;

	public static bool PlayerCanMove {
		get {
			return playerMove;
		}
		set {
			Move(value);
			Call(EVENT.PlayerMoveUpdate);
		}
	}

	////////////////////////////////////////////////////////////////////
	///            REGISTRATION AND EVENT METHODS
	////////////////////////////////////////////////////////////////////

	public static void RegisterEventListener (Action<EVENT> a)
	{
		eventListeners += a;
	}



	////////////////////////////////////////////////////////////////////
	///                   UTILITY METHODS
	////////////////////////////////////////////////////////////////////

	public static void Call (EVENT e)
	{
		if (eventListeners != null) eventListeners(e);
	}

	static void Move (bool val)
	{
		playerMove = val;

		//notify things

	}

}
