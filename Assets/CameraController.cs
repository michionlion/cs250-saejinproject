﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEditor;

public class CameraController : MonoBehaviour
{
	//Mathf.PI * (3f - Mathf.Sqrt(5f));
	public const float GOLDEN_ANGLE = 2.399963229728653322231555506633613853124999f;
	//go no lower than 1 here, doing reconstruction every frame strains GC.
	public const int FIXED_UPDATES_BEFORE_RECONSTRUCTION = 1;
	public const int OBSTACLE_LAYER = 1 << 8;


	public float ObstacleWeight = 1.2f;
	public float InterestDirectionWeight = 0.6f;
	public float DistanceWeight = 1.6f;

	public float MinDistanceToObstacle = 0.5f;
	public float MaxDistanceToObstacle = 3f;
	public float NoObstacleDesirability = 0.5f;
	public float InterestConeDegrees = 120f;
	public float BestDistanceFromFocus = 5f;
	public float BestDistanceFalloffDistance = 3f;
	public float FocusShadowDesirability = 0.1f;
	public float PointCloudRadius = 7f;
	public float PointLocalityDistance = 0.6f;
	public int NumberOfPoints = 2048;

	public float CameraElasticity = 32f;


	DesirabilityMap map;
	int reconstructionCountdown = FIXED_UPDATES_BEFORE_RECONSTRUCTION;

	public Transform MainCamera;
	public float HeightAddition = 1.2f;
	public float InterestAngle;
	protected Vector3 Focus;

	/// <summary>
	/// The walk speed in units per second.
	/// </summary>
	public float WalkSpeed = 3.2f;

	public float RotationSpeed = 30f;

	/// <summary>
	/// Saved movement, x being left-right and y forward-backward movement.
	/// </summary>
	Vector2 movement = new Vector2(0, 0);

	public static bool Enabled = true;


	//calculation vars for reconstruction
	Collider[] obstacles;

	public GameObject Player;

	void Start ()
	{
		if (!Player) Player = GameObject.Find("Player");
		ConstructDMap();
		Update_Camera();
	}

	
	// Update is called once per frame
	void Update ()
	{

		if (Enabled) {
			Update_Camera();
		}

		if (Globals.PlayerCanMove) Update_Move();

		UnityEngine.Debug.DrawRay(Focus, new Vector3(Mathf.Cos(Mathf.Deg2Rad * InterestAngle), 0, Mathf.Sin(Mathf.Deg2Rad * InterestAngle)), Color.green);
	}

	void Update_Move ()
	{
		movement.x += Input.GetAxis("Horizontal") * Time.deltaTime * RotationSpeed;
		movement.y += Input.GetAxis("Vertical") * Time.deltaTime * WalkSpeed;
	}


	void FixedUpdate ()
	{
		if (Enabled) {
			if (--reconstructionCountdown < 0) {
				reconstructionCountdown = FIXED_UPDATES_BEFORE_RECONSTRUCTION;
				Focus.Set(Player.transform.position.x, Player.transform.position.y + HeightAddition, Player.transform.position.z);
				ConstructDMap();
				GC.Collect();
			}
		}

		if (Globals.PlayerCanMove) {
			Player.transform.Rotate(Vector3.up, movement.x, Space.World);
			Player.transform.position += Player.transform.forward * movement.y;
			InterestAngle = -(Player.transform.rotation.eulerAngles.y - 90);
		}

		movement.Scale(Vector2.zero);
	}


	DesirabilityMap.Point dest;
	DesirabilityMap.Point next;

	void Update_Camera ()
	{
		dest = map.GetGlobalMax();

		Queue<DesirabilityMap.Point> q = map.GetLocal(new Vector2(MainCamera.position.x, MainCamera.position.z));
		if (q.Count > 0) next = q.Dequeue();

		while (next != dest) {
			if ((dest.GetPos() - next.GetPos()).sqrMagnitude < (dest.GetPos() - new Vector2(MainCamera.position.x, MainCamera.position.z)).sqrMagnitude) {
				break;
			}
			next = q.Count > 0 ? q.Dequeue() : dest;
		}


		MainCamera.position = Vector3.Lerp(MainCamera.position, new Vector3(dest.GetPos().x, Focus.y, dest.GetPos().y), 1f / CameraElasticity);

		MainCamera.LookAt(Player.transform.position);
	}



	void ConstructDMap ()
	{
		//this needs to take less than 16ms at worst
		//UnityEngine.Debug.Log("Constructing DesirabilityMap!");
		//Stopwatch sw = new Stopwatch();
		//sw.Start();
		//use default values for edge (5), localDistance (0.6), and ambientDesirability (0)
		//well, not now, default for ambient, but edge and local are changable.
		map = new DesirabilityMap(Focus, PointCloudRadius, PointLocalityDistance);

		//initialize the obstacles array, will be reused in each point to avoid excess GC
		obstacles = Physics.OverlapSphere(Focus, map.EdgeDistance * 2);


		int c = 1;
		//const int numPoints = 1024 + 512;

		//use Vogel's method to spread the points on the circle
		while (c < NumberOfPoints) {
			//scale distance by edgeDistance
			//angles in RADIANS
			ConstructPoint((Mathf.Sqrt(c) / Mathf.Sqrt(NumberOfPoints)) * map.EdgeDistance, (GOLDEN_ANGLE * c) % (2 * Mathf.PI));
			c++;
		}

		//sw.Stop();
		//UnityEngine.Debug.Log("Made " + c + " Points in " + sw.ElapsedMilliseconds + "ms!");
	}

	//cut down on miniscule amount of GC. GC really gets killed here by recreating the DesirabilityMap. If there was some way to just modify it...
	float x, z, proxToInterestDir, proxToObstacles, proxToBestDistance;

	void ConstructPoint (float distance, float angle)
	{
		//Debug.Log("Point " + count + " at " + distance + ", " + angle);
		x = distance * Mathf.Cos(angle) + Focus.x;
		z = distance * Mathf.Sin(angle) + Focus.z;

		//max difference is 180, so division by 180 should scale diff 0-1
		proxToInterestDir = 1 - Math.Abs(Mathf.DeltaAngle((angle * Mathf.Rad2Deg) % 360, (InterestAngle % 360) - 180)) / InterestConeDegrees;

		//take into account all colliders within EdgeDistance/2 of this point
		//bitmask 1 << 8 means that only layer 8 will be overlapped with
		//this is the MOST expensive part of reconstruction!
		int cols = Physics.OverlapSphereNonAlloc(Focus, map.EdgeDistance, obstacles, OBSTACLE_LAYER);
		float minDis = Mathf.Infinity;
		float dis;
		for (int i = 0; i < cols; i++) {
			var col = obstacles[i];

			//continue on player collider
			if (col.gameObject.tag.Equals("Player")) continue;

			//assumes mesh matches collider
			dis = NearestVertexToDistance(new Vector3(x, Focus.y, z), col.gameObject);


			if (dis < minDis) minDis = dis;
		}
		proxToObstacles = Mathf.Clamp01((Mathf.Sqrt(minDis) - MinDistanceToObstacle) / (MaxDistanceToObstacle - MinDistanceToObstacle)) * NoObstacleDesirability;



		dis = Mathf.Abs(BestDistanceFromFocus - distance);
		proxToBestDistance = Mathf.Clamp01(1 - dis / BestDistanceFalloffDistance);

		//weighted slightly
		//could customize this, but eh
		float desirability = (proxToInterestDir * InterestDirectionWeight + proxToBestDistance * DistanceWeight + proxToObstacles * ObstacleWeight) / (InterestDirectionWeight + DistanceWeight + ObstacleWeight);


		//do a visibility check
		Vector3 worldPos = new Vector3(x, Focus.y, z);
		RaycastHit hit;
//		if (Physics.Raycast(worldPos, Player.transform.position - worldPos, out hit, (Player.transform.position - worldPos).magnitude, OBSTACLE_LAYER)) {
//			//we hit an obstacle, desirability should not be good!
//			//desirability set to shadow level
//			desirability = FocusShadowDesirability;
//		}
		if (Physics.SphereCast(worldPos, MinDistanceToObstacle, Player.transform.position - worldPos, out hit, (Player.transform.position - worldPos).magnitude, OBSTACLE_LAYER) || Physics.CheckSphere(worldPos, MinDistanceToObstacle + 0.05f)) {
			//we hit an obstacle, desirability should not be good!
			//desirability set to shadow level
			desirability = FocusShadowDesirability;
		}

		//ensure 0-1 ness
		desirability = Mathf.Clamp01(desirability);

		DesirabilityMap.Point p = map.AddPoint(x, z, desirability);
		//UnityEngine.Debug.DrawLine(new Vector3(p.GetPos().x, 0.90f, p.GetPos().y), new Vector3(p.GetPos().x, 0.95f, p.GetPos().y), new Color(0, proxToObstacles, 0), Time.fixedDeltaTime * (FIXED_UPDATES_BEFORE_RECONSTRUCTION + 1));
		//UnityEngine.Debug.DrawLine(new Vector3(p.GetPos().x, 0.95f, p.GetPos().y), new Vector3(p.GetPos().x, 1f, p.GetPos().y), new Color(0, 0, proxToBestDistance), Time.fixedDeltaTime * (FIXED_UPDATES_BEFORE_RECONSTRUCTION + 1));
		Debug.DrawLine(new Vector3(p.GetPos().x, 1f, p.GetPos().y), new Vector3(p.GetPos().x, 1.05f + desirability / 3f, p.GetPos().y), new Color(desirability, 0, 0), Time.fixedDeltaTime * (FIXED_UPDATES_BEFORE_RECONSTRUCTION + 1));
	}

	//super heavy on dense-verticed objects! Not advised for use on >100 or so verts, could slow things down a lot.
	//returns distance squared
	public float NearestVertexToDistance (Vector3 point, GameObject toSearchOn)
	{
		// convert point to local space
		point = toSearchOn.transform.InverseTransformPoint(point);

		float minDistanceSqr = Mathf.Infinity;
		// scan all vertices to find nearest
		foreach (Vector3 vertex in toSearchOn.GetComponent<MeshFilter>().mesh.vertices) {
			Vector3 diff = point - vertex;
			float distSqr = diff.sqrMagnitude;
			if (distSqr < minDistanceSqr) {
				minDistanceSqr = distSqr;
			}
		}
		// convert nearest vertex back to world space
		return minDistanceSqr;

	}

	//	public void OnDrawGizmos ()
	//	{
	//		if (map == null) return;
	//		DesirabilityMap.Point p = map.GetGlobalMax(new Vector2(this.transform.position.x, this.transform.position.z));
	//		if (p == null) return;
	//		Handles.Label(new Vector3(p.GetPos().x, Focus.y, p.GetPos().y), "" + p.Get());
	//		Handles.DrawLine(new Vector3(p.GetPos().x, Focus.y, p.GetPos().y), new Vector3(p.GetPos().x, Focus.y + 0.4f, p.GetPos().y));
	//	}
}
